
class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Brandon"), board = Board.new)
    @player = player
    @board = board
  end

  def attack(pos)
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = nil

    until valid_move?(pos)
      pos = player.get_play
    end

    attack(pos)
  end

  def valid_move?(pos)
    pos.is_a?(Array) && pos.length == 2 && board.in_range?(pos)
  end
end
