class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    puts "Enter move in form: row,col"
    gets.chomp.split(",").map(&:to_i)
  end
end
