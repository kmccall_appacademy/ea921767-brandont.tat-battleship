
class Board
  attr_reader :grid, :length

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def initialize(grid = nil)
    @grid = (grid || self.class.default_grid)
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos
      self[pos] == nil
    else
      count == 0
    end
  end

  def full?
    grid.flatten.count(nil) == 0
  end

  def place_random_ship
    raise_error if full?
    random_pos = [rand(grid.length), rand(grid.length)]
    self[random_pos] = :s if empty?(random_pos)
  end

  def won?
    grid.flatten.all?{|place| place != :s}
  end

  def display
    header = (0...grid.first.length).to_a.join("  ")
    p "  #{header}"
    grid.each_with_index {|row, i| display_row(row, i)}
  end

  def display_row(row, i)
    chars = row.map {|el| [nil, :s].include?(el) ? " " : :x}.join("  ")
    p "#{i} #{chars}"
  end

  def populate_grid
    until count == 10 do
      self.place_random_ship
    end
  end

  def in_range?(pos)
    row, col = pos
    row.between?(0, grid.length-1) && col.between?(0, grid.first.length-1)
  end
end
